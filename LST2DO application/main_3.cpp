#include <iostream>              // cout,cin
#include <fstream>               // ifstream, ofstream
#include <string>                // string
#include <vector>                // vector
#include <algorithm>             // sort
#include "LesData2.h"            // toolbox for reading various data
using namespace std;

/**
* Class Task (with user, taskname, description, deadline, finish, priority,
* start, and status).
*/
class Task {
private:
    string user;
    string taskName;
    string description;
    int    deadline;
    int    finish;
    int    priority;
    int    start;
    bool   status;

public:
    Task() {   lesData();   }
    Task(ifstream & inn);
    virtual void endreData();
    virtual void endrePriority();
    virtual void endreStatus();
    virtual int  hentDeadline() {   return deadline;   }
    virtual int  hentPrio()     {   return priority;   }
    virtual void lesData();
    virtual void skrivData()                const;
    virtual void shortSkrivData()           const;
    virtual void skrivTilFil(ofstream & ut) const;
};

// Global variables
int date     = 0;
int lastTask = 0;


void deleteTask();
void editTask();
void lesFraFil();
void modifyDate(int day);
int  modifyAmDays(int month, int myDay);
void nyTask();
void skrivAlleTasks();
void shortAlleTasks();
void skrivMeny();
void skrivTilFil();
void taskIsDone();


vector <Task*> gTask;          // Vector with task pointers.

/**
* Main program
*/
int main(void) {
    char kommando;
    int month, day, amountDays;

    lesFraFil();

    cout << "\nWelcome to LST2DO!"
         << "\nYour handy to-do-list application!"
         << "\nType '0' to exit different menues.";

    month = lesInt("\n\nWhat month is it?", 1, 12);

    if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8
       || month == 10 || month == 12){amountDays = 31;
    }else if(month == 4 || month == 6 || month == 9 || month == 11){
        amountDays = 30;
    }else{amountDays = 28;}

    day = lesInt("What day of the month is it?", 1, amountDays);

    date = modifyAmDays(month, day);

    cout << "\nDate: "; modifyDate(date);

    skrivMeny();
    kommando = lesChar("\nChoice");

    while (kommando != '0') {
        switch (kommando) {
          case 'S':  skrivAlleTasks();      break;
          case 'N':  nyTask();              break;
          case 'E':  editTask();            break;
          case 'D':  taskIsDone();          break;
          case 'L':  deleteTask();          break;
          default:   skrivMeny();           break;
        }
        skrivMeny();
        kommando = lesChar("\nKommando");
    }
    skrivTilFil();
    return 0;
}

 // ---------------------------------------------------------------------------
 //                      DEFINITION OF CLASS-FUNCTIONS:
 // ---------------------------------------------------------------------------

/**
 * Constructor that reads from file
 * @param inn - fileobject it reads from
 */
Task::Task(ifstream & inn) {
    char check;

    inn.ignore();
    getline(inn, user);             //inn.ignore();
    getline(inn, taskName);         //inn.ignore();
    getline(inn, description);      //inn.ignore();
    inn >> check >> priority >> deadline >> start >> finish;    //inn.ignore();
    status = (check == 1);
}


/**
 *  Gives user options to edit data on tasks
 *  @see  skrivData()
 */
void Task::endreData() {
    int valg;
    int prio;

    // Chose what user wants to edit
    cout <<"\n\tWhat do you want to edit?\n"
         <<"\t1 = Change name of person responsible\n"
         <<"\t2 = Task name and description\n"
         <<"\t3 = Change deadline\n"
         <<"\t4 = Change priority\n"
         <<"\t0 = Nothing, go back\n";

    valg = lesInt("\tChoice: ",0,4);

    switch(valg){
        case 1: cout << "\n\tName of user: ";
                        getline(cin, user);                         break;
        case 2: cout << "\n\tName of task: ";
                        getline(cin, taskName);
                cout << "\n\tDescription of task: ";
                        getline(cin, description);                  break;
        case 3: cout << "\n\tDeadline: ";
                        deadline = lesInt("\n\tDay of the year: ",
                                          date, 365);               break;
        case 4: endrePriority();                                    break;
    }
}

/**
* Changes priority to either
*/
void Task::endrePriority() {
    if(priority == 1){
        priority = 0;
    }
    else{
        priority = 1;
    }
}


void Task::endreStatus() {
    int valg;

    status = true;
    cout << "\n\tStatus was changed to -Done-.\n";

    finish = date;
}



/**
* Reads in a new task
*/
void Task::lesData(){
    int buffer;
    cout << "\n\tName of user: "; getline(cin, user); //reads
    cout << "\n\tName of task: "; getline(cin, taskName);
    cout << "\n\tDescription of task: "; getline(cin, description);
    start = date;
    priority = lesInt("\n\tHigh (1) or Low (0) priority?: ",0,1); //Reads priority
    status = false;
    buffer = lesInt("\n\tDeadline(How many days from today):", 0, 365 - date);
    deadline = date + buffer;                    //task hasnt been complete
 }


 void Task::skrivData() const {

    cout << "\n\t\tTask responsible: " << user;
    cout << "\n\t\tTask name: " << taskName;
    cout << "\n\t\tTask description: " << description;
    cout << "\n\t\tTask priority: " << priority; //Reads priority
    cout << "\n\t\tTask deadline: "; modifyDate(deadline);
    cout << "\n\t\tTask status: " << ((status) ? "Done" : "In progress");
    cout << "\n\t\tTask started: "; modifyDate(start);
    if(finish == 0){
        cout << "\n\t\tTask finished: ";
    }
    else{
        cout << "\n\t\tTask finished: "; modifyDate(finish);
    }
 }

/**
* Prints out the task data
*/
 void Task::shortSkrivData() const {
   cout << "\n\t\tTask name: " << taskName;
   cout << "\n\t\tTask status: " << ((status) ? "Done" : "In progress");
 }

 /**
 * Writes data to file
 * @param ut - fileobject it reads to
 */
 void Task::skrivTilFil(ofstream & ut) const {
    ut << user << '\n' << taskName << '\n' << description << '\n'
       << ((status) ? "1" : "0") << ' ' << priority << ' ' << deadline
       << ' ' << start << ' ' << finish << '\n';
 }


// ---------------------------------------------------------------------------
//                     DEFINITION OF OTHER FUNCTIONS:
// ---------------------------------------------------------------------------

/**
* Delete task
* @see shortAlleTasks()
*/
void deleteTask(){
    shortAlleTasks();

    int choice;
    choice = lesInt("\n\tWhat task do you want to delete?", 0, gTask.size());


    if(choice != 0){
        --choice;
        gTask.erase(gTask.begin() + choice);
        --lastTask;
        cout << "\tTask nr." << choice << " was deleted.\n";
    }

}

/**
 * Allows the user (if possible) to modify the data for a subclass.
 * @see virtual Kjoretoy::endreData()
 * @see shortAlleTasks()
 */
void editTask() {
    shortAlleTasks();

    if(gTask.size() > 0){
        int valgNr = lesInt("\n\tWhat's the ID of the Task you want to edit?: ",
                            0, gTask.size());

        if(valgNr != 0){
            --valgNr;
            gTask[valgNr]->endreData();
        }
    }
    else {
        cout << "\n\tNo Tasks registered on system\n";
    }
}

/**
* Reads data from file and create task object
* @see hentProo()
*/
void lesFraFil() {
    ifstream innfil("LST2DO.DTA");

    if(innfil) {
        cout << "Reading from file 'LST2DO.DTA'......\n\n";
        innfil >> lastTask;

        for(int i = 0; i < lastTask; i++){
            Task* nyTask;
            nyTask = new Task(innfil);
            gTask.push_back(nyTask);
        }

        sort(gTask.begin(), gTask.end(), []( Task* t1,  Task* t2) //Sort by priority
                           {  return (t1->hentPrio() > t2->hentPrio());  });

        innfil.close();
    }
    else{
        cout << "\n\nCouldn't find the file named 'LST2DO.DTA'!\n\n";
    }
}

/**
* Figures out what month it is
*/
void modifyDate(int day) {
    int exDay;

    if(day <= 365 && day > 334){
        exDay = day - 334;
        cout <<  exDay << ". December";
    }
    else if(day < 335 && day > 304){
        exDay = day - 304;
        cout  << exDay << ". November";
    }
    else if(day < 305 && day > 273){
        exDay = day - 273;
        cout << exDay << ". October";
    }
    else if(day < 274 && day > 243){
        exDay = day - 243;
        cout << exDay << ". September";
    }
    else if(day < 244 && day > 212){
        exDay = day - 212;
        cout << exDay << ". August";
    }
    else if(day < 213 && day > 181){
        exDay = day - 181;
        cout << exDay << ". July";
    }
    else if(day < 182 && day > 151){
        exDay = day - 151;
        cout << exDay << ". June";
    }
    else if(day < 152 && day > 120){
        exDay = day - 120;
        cout << exDay << ". May";
    }
    else if(day < 121 && day > 90){
        exDay = day - 90;
        cout << exDay << ". April";
    }
    else if(day < 91 && day > 59){
        exDay = day - 59;
        cout << exDay << ". March";
    }
    else if(day < 60 && day > 31){
        exDay = day - 31;
        cout << exDay << ". February";
    }
    else if(day < 32 && day > 0){
        exDay = day - 0;
        cout << exDay << ". January";
    }
}

/**
* Figures out how many days in each month
*/
int modifyAmDays(int month, int myDay){
    int sum, day;

    if (month ==1)
    {
        day=0;
        sum=day + myDay;
        return sum;
    }
    else if (month==2)
    {
        day=31;
        sum=day + myDay;
        return sum;
    }
    else if (month==3)
    {
        day=59;
        sum=day + myDay;
        return sum;
    }
    else if (month==4)
    {
        day=90;
        sum=day + myDay;
        return sum;
    }
    else if (month==5)
    {
        day=120;
        sum=day + myDay;
        return sum;
    }
    else if (month==6)
    {
        day=151;
        sum=day + myDay;
        return sum;
    }
    else if (month==7)
    {
        day=181;
        sum=day + myDay;
        return sum;
    }
    else if (month==8)
    {
        day=212;
        sum=day + myDay;
        return sum;
    }
    else if (month==9)
    {
        day=243;
        sum=day + myDay;
        return sum;
    }
    else if (month==10)
    {
        day=273;
        sum=day + myDay;
        return sum;
    }
    else if (month==11)
    {
        day=304;
        sum=day + myDay;
        return sum;
    }
    else if (month==12)
    {
        day=334;
        sum=day + myDay;
        return sum;
    }
}

/**
* Create new task
* @see hentPrio()
* @see Task::lesData()
*/
void nyTask() {
    Task* nyTask;
    nyTask = new Task();
    gTask.push_back(nyTask);
     ++lastTask;

    sort(gTask.begin(), gTask.end(), []( Task* t1,  Task* t2) //Sort by priority
                           {  return (t1->hentPrio() > t2->hentPrio());  });

    cout << "\tNew task has ID." << lastTask << ":\n";

    cout << "\n\tTask registered!\n";
}

/**
* Writes all data for each task
* @see Task::skrivData()
*/
void skrivAlleTasks() {
    for(int i = 0; i < gTask.size(); i++){
        cout << "\n\tTask nr." << i + 1;
        gTask[i]->skrivData();
        cout << "\n";
    }
}

/**
* Wtites some data for each task
* @see shortSkrivData()
*/
void shortAlleTasks() {
    for(int i = 0; i < gTask.size(); i++){
        cout << "\n\tTask nr." << i + 1;
        gTask[i]->shortSkrivData();
        cout << "\n";
    }
}

/**
 *  Writes the program's menu selections / options on the screen.
 */
void skrivMeny() {
    cout << "\n\nMAIN MENU:\n"
         << "\t(S) - (S)how all tasks\n"
         << "\t(N) - Register (n)ew task\n"
         << "\t(E) - (E)dit task details\n"
         << "\t(D) - Mark task as (d)one\n"
         << "\t(L) - De(l)ete task\n"
         << "\t(0) - Exit the program\n";
}

/**
* Writes task information to file
* @see Task::skrivTilFil(utfil)
*/
void skrivTilFil() {
    ofstream utfil("LST2DO.DTA");   //Bruker .DT2 istedet for .DTA for testing

    cout << "\nWriting to file 'LST2DO.DTA' .....\n\n";

    utfil << lastTask << '\n';

    for(int i = 0; i < lastTask; i++){
        gTask[i]->skrivTilFil(utfil);
    }

    utfil.close();                          //Lukker filen manuelt.
}

/**
* Changes status from "in progress" to "done" and oposite
* @see Task::shortAlleTasks()
* @see Task::endreStatus()
*/
void taskIsDone(){
    char valg;
    shortAlleTasks();

    if(gTask.size() > 0){
        int valgNr =
            lesInt("\n\tWhat's the Task you want to edit status of?: ", 0, gTask.size());

        if(valgNr != 0){
            --valgNr;
            gTask[valgNr]->endreStatus();
        }

        do{
            valg = lesChar("\n\tDo you want to remove this task from the list?(Y/N)");
        }while(valg != 'Y' && valg != 'N');

        if(valg == 'Y'){
            gTask.erase(gTask.begin() + valgNr);
            --lastTask;
            cout << "\n\tTask nr." << valgNr << " was deleted.\n";
        }
    }
    else {
        cout << "\n\tNo Tasks registered on system\n";
    }
}
